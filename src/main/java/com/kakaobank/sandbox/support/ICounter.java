package com.kakaobank.sandbox.support;

public interface ICounter {

    Long getAndIncrement();

}
