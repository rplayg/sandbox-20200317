package com.kakaobank.sandbox.util;

import com.kakaobank.sandbox.support.ICounter;

import java.util.concurrent.atomic.AtomicLong;

public class ThreadLocalCounter implements ICounter {

    private ThreadLocal<AtomicLong> counter = ThreadLocal.withInitial(() -> new AtomicLong());

    @Override
    public Long getAndIncrement() {
        return counter.get().getAndIncrement();
    }
}
