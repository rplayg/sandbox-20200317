package com.kakaobank.sandbox.util;

import com.kakaobank.sandbox.support.ICounter;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicCounter implements ICounter {

    private static final AtomicLong counter = new AtomicLong();

    @Override
    public Long getAndIncrement() {
        return counter.getAndIncrement();
    }
}
