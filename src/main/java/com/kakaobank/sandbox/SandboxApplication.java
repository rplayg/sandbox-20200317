package com.kakaobank.sandbox;

import com.kakaobank.sandbox.support.ICounter;
import com.kakaobank.sandbox.util.AtomicCounter;
import com.kakaobank.sandbox.util.ThreadLocalCounter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SandboxApplication {

    public static void main(String[] args) {
        SpringApplication.run(SandboxApplication.class, args);
    }

    @Bean
    public ICounter atomicCounter() {
        return new AtomicCounter();
    }

    @Bean
    public ICounter threadLocalCounter() {
        return new ThreadLocalCounter();
    }

}
