package com.kakaobank.sandbox.service;

import com.kakaobank.sandbox.support.ICounter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
public class IdGenerationService {

    private final ICounter atomicCounter;

    private final ICounter threadLocalCounter;

    @Autowired
    public IdGenerationService(ICounter atomicCounter, ICounter threadLocalCounter) {
        this.atomicCounter = atomicCounter;
        this.threadLocalCounter = threadLocalCounter;
    }

    public String generateByAtomicCounter() {
        final String count = StringUtils.leftPad(atomicCounter.getAndIncrement().toString(), 6, "0");
        return yyyyMMddHHmmss() + count;
    }

    public String generateByThreadLocalCounter() {
        final String threadId = StringUtils.leftPad(Long.toHexString(Thread.currentThread().getId()), 3, "0");
        final String count = StringUtils.leftPad(threadLocalCounter.getAndIncrement().toString(), 3, "0");
        return yyyyMMddHHmmss() + threadId + count;
    }

    private String yyyyMMddHHmmss() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

}
