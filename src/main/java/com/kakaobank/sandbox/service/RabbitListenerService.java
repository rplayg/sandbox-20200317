package com.kakaobank.sandbox.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Slf4j
@Service
public class RabbitListenerService {

    private final IdGenerationService idGenerationService;

    private final SetOperations<String, String> setOperations;

    public RabbitListenerService(IdGenerationService idGenerationService, StringRedisTemplate redisTemplate) {
        this.idGenerationService = idGenerationService;
        this.setOperations = redisTemplate.opsForSet();
    }

    @RabbitListener(queues = "atomic")
    public void idGenerationByAtomic(final Message message) {
        log.debug(getMessageBody(message));
        final String uid = idGenerationService.generateByAtomicCounter();
        final Long res = setOperations.add("UID:ATOMIC", uid);
        if (res != 1l) {
            log.warn("Method: {}, UID: {} duplicated", "atomic", uid);
        }
    }

    @RabbitListener(queues = "thread_local")
    public void idGenerationByThreadLocal(final Message message) {
        log.debug(getMessageBody(message));
        final String uid = idGenerationService.generateByThreadLocalCounter();
        final Long res = setOperations.add("UID:THREAD_LOCAL", uid);
        if (res != 1l) {
            log.warn("Method: {}, UID: {} duplicated", "thread_local", uid);
        }
    }

    private String getMessageBody(Message message) {
        return new String(message.getBody(), StandardCharsets.UTF_8);
    }

}
