package com.kakaobank.sandbox.controller;

import com.kakaobank.sandbox.service.IdGenerationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@RequestMapping("/uid")
@RestController
public class UidController {

    private final IdGenerationService idGenerationService;

    private final SetOperations<String, String> setOperations;

    @Autowired
    public UidController(IdGenerationService idGenerationService, StringRedisTemplate redisTemplate) {
        this.idGenerationService = idGenerationService;
        this.setOperations = redisTemplate.opsForSet();
    }

    @GetMapping("/by_atomic")
    public String atomic() {
        final String uid = idGenerationService.generateByAtomicCounter();
        final Long res = setOperations.add("UID:ATOMIC", uid);
        if (res < 1l) {
            log.warn("Method: {}, UID: {} duplicated", "atomic", uid);
        }
        return uid;
    }

    @GetMapping("/by_thread_local")
    public String threadLocal() {
        final String uid = idGenerationService.generateByThreadLocalCounter();
        final Long res = setOperations.add("UID:THREAD_LOCAL", uid);
        if (res < 1l) {
            log.warn("Method: {}, UID: {} duplicated", "thread_local", uid);
        }
        return uid;
    }

    @GetMapping("/tp/atomic")
    public String executeOnThreadPool() throws InterruptedException {
        final LocalDateTime start = LocalDateTime.now();
        final ExecutorService executorService = Executors.newFixedThreadPool(50);
        while(true) {
            executorService.submit(() -> {
                final String uid = idGenerationService.generateByAtomicCounter();
                final Long res = setOperations.add("UID:ATOMIC", uid);
                log.info("Method: atomic, uid:{}, res:{}", uid, res);
                if (res < 1l) {
                    log.warn("Method: {}, UID: {} duplicated", "atomic", uid);
                }
            });
            log.info("submit!");
            final LocalDateTime end = LocalDateTime.now();
            if (Duration.between(start, end).getSeconds() > 60l) {
                log.warn("60 seconds break!");
                break;
            }
            Thread.sleep(30l);
        }
        return "OK";
    }

}
