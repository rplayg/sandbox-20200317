package com.kakaobank.sandbox.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistry;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@Slf4j
@RequestMapping("/rabbit")
@RestController
public class RabbitController {

    private final RabbitTemplate rabbitTemplate;

    private final RabbitListenerEndpointRegistry rabbitListenerEndpointRegistry;

    @GetMapping("/{routing_key}/publish")
    public String publish(@PathVariable("routing_key") String routingKey) {
        final String payload = RandomStringUtils.randomAlphanumeric(20);
        rabbitTemplate.convertAndSend("sandbox", routingKey, payload);
        return payload;
    }

    @GetMapping("/stop")
    public void stop() {
        rabbitListenerEndpointRegistry.stop(() -> {log.info("rabbit listener stopped!!");});
    }

    @GetMapping("/start")
    public void start() {
        rabbitListenerEndpointRegistry.start();
    }

}
