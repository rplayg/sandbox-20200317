package com.kakaobank.sandbox.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class RabbitConfig {

    @Bean
    public Declarables defaultBinding() {
        final Queue atomicQueue = new Queue("atomic");
        final Queue threadLocalQueue = new Queue("thread_local");
        final Exchange exchange = new DirectExchange("sandbox");

        return new Declarables(exchange, atomicQueue, threadLocalQueue,
                BindingBuilder.bind(atomicQueue).to(exchange).with("atomic").noargs(),
                BindingBuilder.bind(threadLocalQueue).to(exchange).with("thread_local").noargs());
    }

}
